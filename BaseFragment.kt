package com.example

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.navigation.NavOptions
import com.example.BR
import com.example.view.common.actions.Actions
import com.example.view.common.actions.OnBackClicked
import com.example.view.ui.base.BaseActivity
import com.example.view.ui.base.presentation.BaseViewModel
import com.example.view.ui.main.view.MainActivity
import timber.log.Timber

abstract class BaseFragment<T : ViewDataBinding, out V : BaseViewModel> : Fragment() {

    protected abstract fun getViewModels(): V?
    protected open fun getBindingVariable(): Int = BR.viewModel
    private lateinit var viewDataBinding: T

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewDataBinding = DataBindingUtil.inflate(inflater, getLayout(), container, false)
        viewDataBinding.lifecycleOwner = this

        viewDataBinding.setVariable(getBindingVariable(), getViewModels()?.apply {
            lifecycle.addObserver(this)
        })
        viewDataBinding.executePendingBindings()

        return viewDataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewReady()
    }

    override fun onStart() {
        super.onStart()
        getViewModels()?.actions?.observe(this, { it?.let { onActions(it) } })
    }

    override fun onStop() {
        super.onStop()
        getViewModels()?.actions?.removeObservers(this)
    }

    open fun onActions(action: Actions) {
        Timber.w(action.toString())
        when (action) {
            is OnBackClicked -> onBackPressed()
        }
    }

    protected fun safeNavigation(action: Int, bundle: Bundle? = null) {
        if (isAdded && isVisible) {
            val navigationController = getParent<MainActivity>()?.navigationController()
            var navOptions: NavOptions? = null

            if (navigationController?.currentDestination?.id != getNavId()) {
                navOptions = NavOptions.Builder()
                    .setPopUpTo(getNavId(), false)
                    .build()
            }

            navigationController?.navigate(action, bundle, navOptions)
        }
    }

    protected fun onBackPressed() = (activity as BaseActivity).onBackPressed()

    abstract fun viewReady()

    abstract fun getLayout(): Int

    abstract fun getNavId(): Int

    inline fun <reified T : BaseActivity> getParent(): T? = activity as? T
}