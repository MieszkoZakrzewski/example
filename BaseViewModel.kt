package com.example

import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.view.common.actions.Actions
import com.example.view.common.actions.OnBackClicked
import org.koin.core.component.KoinComponent


abstract class BaseViewModel : ViewModel(), DefaultLifecycleObserver, KoinComponent {
    val actions = MutableLiveData<Actions>()
    val persistableActions = MutableLiveData<Actions>()

    override fun onStart(owner: LifecycleOwner) {
        super.onStart(owner)
        initPropertyChangedCallbacks()
    }

    open fun initPropertyChangedCallbacks() {}

    override fun onStop(owner: LifecycleOwner) {
        super.onStop(owner)
        tearDownPropertyChangedCallbacks()
    }

    open fun tearDownPropertyChangedCallbacks() {}

    fun dispatchPersistableAction(action: Actions) {
        persistableActions.postValue(action)
    }

    fun dispatchAction(action: Actions) {
        actions.value = action
        actions.value = null
    }

    fun dispatchActionPost(action: Actions) {
        actions.postValue(action)
    }

    fun back() {
        dispatchAction(OnBackClicked)
    }
}